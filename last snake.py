# -*- coding: utf-8 -*-
"""
Created on Tue May  8 21:51:53 2018

@author: Mahsa Riyahi
"""

import pygame

pygame.init()

w,h = 500,500
screen = pygame.display.set_mode((w,h))

c = pygame.time.Clock()


running = True

screen.fill((255,255,255))
counter = 0

x1 , x2 , x3 = 80 , 100 , 120
y1 , y2 , y3 = 80 , 80 , 80
r = 10
move = 20
MOVE=20
jahat=0
climax = [ (x1 , y1 ) ,  (x2 , y2),  (x3 , y3)]
snake_color = (0,255,0)


while running:   
    c.tick(20)
    screen.fill((0, 0, 0))
    

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            running = False
            
    pygame.draw.circle(screen , snake_color , (climax[0]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[1]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[2]), r , 0)
        
    key = pygame.key.get_pressed()
    if key[pygame.K_LEFT]:
        jahat=1
        del climax[2]
        climax.insert(2 , climax[1] )
        climax.insert(1 , climax[0] )
        climax.insert(0 , (climax[0][0]-move,climax[0][1]) )
       
    pygame.draw.circle(screen , snake_color , (climax[0]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[1]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[2]) , r , 0)
    
    if key[pygame.K_UP]:
        jahat=2
        del climax[2] 
        climax.insert(2 , climax[1] )
        climax.insert(1 , climax[0] )
        climax.insert(0 , (climax[0][0],climax[0][1]-move) )
       
    pygame.draw.circle(screen , snake_color , (climax[0]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[1]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[2]) , r , 0)
   
    if key[pygame.K_DOWN]:
        jahat=3
        del climax[2]
        climax.insert(2 , climax[1] )
        climax.insert(1 , climax[0] )
        climax.insert(0 , (climax[0][0],climax[0][1]+move) )
        
    pygame.draw.circle(screen , snake_color , (climax[0]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[1]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[2]) , r , 0)
   
    if key[pygame.K_RIGHT]:
        jahat=4
        del climax[2]
        climax.insert(2 , climax[1] )
        climax.insert(1 , climax[0] )
        climax.insert(0 , (climax[0][0]+move,climax[0][1]) )
       
    pygame.draw.circle(screen , snake_color , (climax[0]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[1]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[2]) , r , 0)
    
    if jahat==1:
        del climax[2]
        climax.insert(2 , climax[1] )
        climax.insert(1 , climax[0] )
        climax.insert(0 , (climax[0][0]-move,climax[0][1]) )
       
    pygame.draw.circle(screen , snake_color , (climax[0]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[1]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[2]) , r , 0)
    
        
    if jahat==2:
        del climax[2] 
        climax.insert(2 , climax[1] )
        climax.insert(1 , climax[0] )
        climax.insert(0 , (climax[0][0],climax[0][1]-move) )
       
    pygame.draw.circle(screen , snake_color , (climax[0]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[1]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[2]) , r , 0)
        
    if jahat==3:
        del climax[2]
        climax.insert(2 , climax[1] )
        climax.insert(1 , climax[0] )
        climax.insert(0 , (climax[0][0],climax[0][1]+move) )
        
    pygame.draw.circle(screen , snake_color , (climax[0]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[1]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[2]) , r , 0)
        
    if jahat==4:
        del climax[2]
        climax.insert(2 , climax[1] )
        climax.insert(1 , climax[0] )
        climax.insert(0 , (climax[0][0]+move,climax[0][1]) )
       
    pygame.draw.circle(screen , snake_color , (climax[0]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[1]) , r , 0)
    pygame.draw.circle(screen , snake_color , (climax[2]) , r , 0)

        
    pygame.display.update()
pygame.quit()
quit() 